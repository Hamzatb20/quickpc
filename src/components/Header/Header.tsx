import { IonApp, IonButtons, IonContent, IonHeader, IonIcon, IonSearchbar, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';
import { tvOutline } from 'ionicons/icons'
import { bag } from 'ionicons/icons'
import { heart } from 'ionicons/icons'
import { logIn } from 'ionicons/icons'
import "./Header.css"
import Login from '../Login/Login';
import Register from '../Register/Register';


const Header: React.FC = () => {
    const [searchText, setSearchText] = useState('');
    return (
        <div>
            <IonHeader class="header-class">
                <IonToolbar class="toolbar-class">
                    <IonIcon icon={tvOutline} class="iconPC" />
                    <IonTitle class="title-text">QuickPC</IonTitle>
                    <IonSearchbar class="search-class" value={searchText} onIonChange={e => setSearchText(e.detail.value!)}></IonSearchbar>
                    <IonIcon icon={bag} class="bag-class-icon" />
                    <IonIcon icon={heart} class="bag-class-icon" />
                    <IonButtons class='log-reg-class'>
                        <Login />
                        <Register />
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
        </div>
    );
};

export default Header;