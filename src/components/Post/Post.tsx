import { IonButton, IonCol, IonContent, IonGrid, IonHeader, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import styles from './Post.module.scss';

interface IMyProps {
    message: string,
    name: string,
}

const Post: React.FC<IMyProps> = (props: IMyProps) => {
    return (
        <div>
            <div className='s.item'>
                <img src="https://i.natgeofe.com/n/9135ca87-0115-4a22-8caf-d1bdef97a814/75552.jpg?w=636&h=424" alt="Avatar" width="90" />
                <p><span>{props.name}</span></p>
                <p>{props.message}</p>

            </div>
            <div className='s.item'>
                <img src="https://images.theconversation.com/files/377334/original/file-20210106-15-jyki6r.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1200&h=1200.0&fit=crop" alt="Avatar" width="90" />
                <p><span>{props.name}</span></p>
                <p>{props.message}</p>
            </div>
            {/* <div className={styles.container}>
                <img src="https://i.natgeofe.com/n/9135ca87-0115-4a22-8caf-d1bdef97a814/75552.jpg?w=636&h=424" alt="Avatar" width="90" />
                <p><span>Тахаев Усам</span>Сеньор помидор</p>
                <p>Компы четкие брат</p>
            </div>

            <div className={styles.container}>
                <img src="https://images.theconversation.com/files/377334/original/file-20210106-15-jyki6r.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1200&h=1200.0&fit=crop" alt="Avatar" width="90" />
                <p><span>Мурдалов Магомед</span>Мидл повидл</p>
                <p>10 из 10</p>
            </div> */}
        </div>
    );
};

export default Post;
