import { IonAlert, IonButton, IonCol, IonContent, IonGrid, IonHeader, IonInput, IonModal, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';
import Post from '../Post/Post';
import styles from './Review.module.scss';


const Review: React.FC = () => {

  const [showModal, setShowModal] = useState(false);
  const [inputText, setInputText] = useState<string>('');

  return (
    <div>
      <IonModal isOpen={showModal} cssClass='my-custom-class'>
        <IonInput />
        <p>This is modal content</p>
        <IonButton onClick={() => setShowModal(false)}>Close Modal</IonButton>
      </IonModal>
      
      <IonTitle className={styles.title}>Отзывы</IonTitle>
      <IonButton onClick={() => setShowModal(true)}>Написать отзыв</IonButton>
      <Post message={inputText} name={inputText} />
      <Post message={inputText} name={inputText} />
    </div>
  );
};

export default Review;
