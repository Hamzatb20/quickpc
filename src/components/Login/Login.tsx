import { IonAlert, IonButton, IonList, IonContent, IonGrid, IonHeader, IonInput, IonItem, IonModal, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';
import styles from './Login.module.scss';


const Login: React.FC = () => {

    const [showModal, setShowModal] = useState(false);
    const [login, setLogin] = useState<string>();
    const [password, setPassword] = useState<string>();

    return (
        <div>
            <IonModal isOpen={showModal} cssClass='my-custom-class'>
                <IonList>
                    <IonInput value={login} placeholder="Логин/Почта" onIonChange={e => setLogin(e.detail.value!)}></IonInput>
                    <IonInput type='password' value={password} placeholder="Пароль" onIonChange={e => setPassword(e.detail.value!)}></IonInput>
                </IonList>
                <IonButton onClick={() => setShowModal(false)}>Войти</IonButton>
            </IonModal>
            <IonButton className={styles.loginClass} color='dark' onClick={() => setShowModal(true)}>Войти</IonButton>
        </div>
    );
};

export default Login;
