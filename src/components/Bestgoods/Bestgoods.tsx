import { IonApp, IonContent, IonHeader, IonIcon, IonSearchbar, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';
import "./Bestgoods.css"


const Bestgoods: React.FC = () => {
    return (
        <div>
            <IonTitle className='bestgoods-title-class'>Лучшие товары</IonTitle>
            <div className='or'>
                <div>
                    <img className='orr' src="https://ru.store.asus.com/image/cache/catalog/notebooks-3/rog/g512LWS_LW_LV_LU_Original_Black_3fin_numpad_4zone-14-Light-470x470.jpg" />
                </div>
                <div>
                    <img className='orr' src="https://shop.lenovo.ru/upload/resize_cache/content/1120_1120_17f5c944b3b71591cc9304fac25365de2/iblock/03f/03f5359f510fda8df621ea3aa8cc6aa5.jpg" />
                </div>
                <div>
                    <img className='orr' src="https://img.mvideo.ru/Pdb/30048053b.jpg" />
                </div>
                <div>
                    <img className='orr' src="https://items.s1.citilink.ru/1217402_v01_b.jpg" />
                </div>
            </div>
        </div>
    );
};

export default Bestgoods;