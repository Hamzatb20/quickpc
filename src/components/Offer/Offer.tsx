import { IonApp, IonContent, IonHeader, IonIcon, IonSearchbar, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';
import "./Offer.css"
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"></link>

const Offer: React.FC = () => {
    return (
        <div>
            <IonTitle className='offer-title-class'>Предложение дня</IonTitle>
            <div className='offer-of-the-day-class'>
                <img src="https://mediasat.info/wp-content/uploads/2020/04/pc.jpg" />
                <div>ПК DEXP Mars E317 [Intel Core i3 9100F, 4x3600 МГц, 8 ГБ DDR4, GeForce GTX 1050 Ti, SSD 512 ГБ, без ОС]</div>
                <div className="star-rating">
                    <div className="star-rating__wrap">
                        <input className="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" />
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-5" title="5 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" />
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-4" title="4 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" />
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-3" title="3 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" />
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-2" title="2 out of 5 stars"></label>
                        <input className="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" />
                        <label className="star-rating__ico fa fa-star-o fa-lg" htmlFor="star-rating-1" title="1 out of 5 stars"></label>
                    </div>
                </div>
                <div>Цена: 95 000</div>
            </div>
        </div>
    );
};

export default Offer;