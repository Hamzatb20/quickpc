import { IonAlert, IonList, IonApp, IonLabel, IonButton, IonCol, IonContent, IonGrid, IonHeader, IonInput, IonModal, IonPage, IonItem, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';

const Register: React.FC = () => {

    const [showModal, setShowModal] = useState(false);
    const [login, setLogin] = useState<string>();
    const [password, setPassword] = useState<string>();
    const [name, setName] = useState<string>();
    const [lastName, setLastName] = useState<string>();
    const [birthDate, setBirthDate] = useState<string>();

    return (
        <div>
            <IonModal isOpen={showModal} cssClass='my-custom-class'>
                <IonList>
                    <IonInput value={name} placeholder="Имя" onIonChange={e => setName(e.detail.value!)}></IonInput>
                    <IonInput value={lastName} placeholder="Фамилия" onIonChange={e => setLastName(e.detail.value!)}></IonInput>
                    <IonInput type='date' value={birthDate} placeholder="Дата рождения" onIonChange={e => setBirthDate(e.detail.value!)}></IonInput>
                    <IonInput value={login} placeholder="Логин/Почта" onIonChange={e => setLogin(e.detail.value!)}></IonInput>
                    <IonInput type='password' value={password} placeholder="Пароль" onIonChange={e => setPassword(e.detail.value!)}></IonInput>
                </IonList>
                <IonButton onClick={() => setShowModal(false)}>Зарегистрироваться</IonButton>
            </IonModal>
            <IonButton color='dark' onClick={() => setShowModal(true)}>Регистрация</IonButton>
        </div>
    );
};

export default Register;