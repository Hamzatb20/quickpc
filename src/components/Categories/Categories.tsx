import { IonApp, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonSearchbar, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';
import "./Categories.css"


const Categories: React.FC = () => {
    return (
        <div>
            {/* <IonItem class="categ-class" >
                <IonLabel>
                    Категории
                </IonLabel>
            </IonItem> */}
            <nav className="nav">
                <IonTitle className='categor-title-class'>Categories</IonTitle>
                <div className='item'>
                    <a>Персональные компьютеры</a>
                </div>
                <div className='item'>
                    <a>Ноутбуки</a>
                </div>
                <div className='item'>
                    <a>Мониторы</a>
                </div>
                <div className='item'>
                    <a>Запчасти и комплектующие</a>
                </div>
                <div className='item'>
                    <a>Аксессуары</a>
                </div>
                <div className='item'>
                    <a>Сетевое оборудование</a>
                </div>
                <div className='item'>
                    <a>Услуги</a>
                </div>
                <div className='item'>
                    <a>Кабели и разъемы</a>
                </div>
            </nav>
        </div>
    );
};

export default Categories;