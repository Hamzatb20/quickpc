import { IonApp, IonContent, IonHeader, IonIcon, IonImg, IonSearchbar, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';
import styles from "./ProductList.module.scss"



interface IProductListProps {
    products: IProduct[];
}

export interface IProduct {
    name: string,
    price: number,
    imageUrl: string,
}


const PropductList: React.FC<IProductListProps> = (props) => {
    return (
        <div className={styles.conteiner}>
            {
                props.products.map(x => (
                <div className={styles.item}>
                  <img className={styles.img} src={x.imageUrl}/>
                  <div>{x.name}-<span className={styles.price}>{x.price}</span></div>
                </div>  ))
            }
        </div>


    );
};

export default PropductList;