import { IonApp, IonContent, IonHeader, IonIcon, IonSearchbar, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';
import "./Zagolovok.css"


const Zagolovok: React.FC = () => {
    return (
        <IonApp>
            <div className='parent'>
                <div className='categor-class'>
                    Категории
                </div>
                <div className='best-class'>
                    Лучшие товары
                </div>
                <div className='offer-class'>
                    Предложение дня
                </div>
            </div>
        </IonApp>
    );
};

export default Zagolovok;