import { IonButton, IonCol, IonContent, IonGrid, IonHeader, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';
import Header from '../components/Header/Header';
import Categories from '../components/Categories/Categories'
import './Home.css';
import Bestgoods from '../components/Bestgoods/Bestgoods';
import Offer from '../components/Offer/Offer';
import PropductList, { IProduct } from '../components/ProductList/ProductList';
import Sale from '../components/Sale/Sale';
import Review from '../components/Review/Review';
import Login from '../components/Login/Login';
import Register from '../components/Register/Register';
import { useHistory } from 'react-router-dom';

const dymmiDate: IProduct[] = [
  {
    name: "Comp",
    price: 300,
    imageUrl: "https://items.s1.citilink.ru/1217402_v01_b.jpg"
  }, {
    name: "222",
    price: 3241,
    imageUrl: "https://items.s1.citilink.ru/1217402_v01_b.jpg"
  }, {
    name: "2223",
    price: 300,
    imageUrl: "https://items.s1.citilink.ru/1217402_v01_b.jpg"
  }, {
    name: "3333",
    price: 21312,
    imageUrl: "https://items.s1.citilink.ru/1217402_v01_b.jpg"
  },
]


const Home: React.FC = () => {
  return (
    <div>
      <Header />
      <div>
        <IonGrid>
          <IonRow>
            <IonCol size="3">
              <Categories />
            </IonCol>
            <IonCol size="5">
              <Bestgoods />
              <Sale />
            </IonCol>
            <IonCol size="4">
              <Offer />
            </IonCol>
          </IonRow>
        </IonGrid>
      </div>
      <Review />
      {/* <IonTitle className='review-title'>Отзывы</IonTitle>
      <IonButton>Написать отзыв</IonButton> */}
      {/* <PropductList products={dymmiDate} /> */}
    </div>
  );
};

export default Home;
